<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps = false;
    public function Category(){
        return $this->belongsTo('App\\Category','category_id');
    }
}
