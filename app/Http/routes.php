<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
Route::get('/', function () {
    return view("welcome");
});

Route::post('/auth/login',"Auth\\AuthController@postLogin");

Route::get('/signin', ['as'=>'login',function () {
    $error=Session::get('error');
    return View::make('auth/login')->with('error',$error);
}]);

Route::get('/register', ['as'=>'register',function () {
    return View::make('auth/reg');
}]);

Route::any('/auth/logout', function () {
    Auth::logout();
    return redirect()->route('login');
});

Route::post('/auth/reg',"Auth\\AuthController@reg");


Route::get('/index',"UserController@index");
Route::get('/contacts',"UserController@contacts");
Route::get('/contactsByCat/{id}',"UserController@contactsByCat");
Route::get('/message',"UserController@message");
Route::post('/send',"UserController@send");

Route::any('/{i}',function(){
    if(Auth::check()) return redirect('/index');
    else return redirect('/signin');
})->where(['i'=>'.*']);

