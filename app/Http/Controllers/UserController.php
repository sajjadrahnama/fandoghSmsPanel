<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contact;
use App\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $user=Auth::user();
        if($user==null) return redirect("/signin");
        $messages=Message::where("sender_id","=",$user->id)->get()->toArray() ;
        return View::make('userIndex')->with('messages', $messages);
    }

    public function contacts(){
        $user=Auth::user();
        if($user==null) return redirect("/signin");
        $cat=Category::all()->toArray();
        return View::make('contacts')->with("cats",$cat);
    }


    //      Ajax Uses
    public function contactsByCat($catId){
        $user=Auth::user();
        if($user==null) return "";
        $contacts=Contact::where("category_id","=",$catId)->get()->toArray();
        return View::make('contactsByCatAjax')->with('contacts',$contacts);
    }

    public function message(){
        $user=Auth::user();
        if($user==null) return redirect("/signin");
        $tree=array();
        $temp=Category::all();

        foreach($temp as $c ){
            $contact=Contact::where("category_id","=",$c->id)->get();
            $tree[]=new \stdClass();
            end($tree)->text=$c->name;
            end($tree)->selectable=false;
            end($tree)->nodes=array();
            foreach ($contact as $cont ) {
                end($tree)->nodes[]=new \stdClass();
                end(end($tree)->nodes)->text=$cont->first_name." ".$cont->last_name." ".$cont->phone_number;
                end(end($tree)->nodes)->selectable=false;
            }


        }
        $res=json_encode($tree);
        return View::make('sendMessage')->with('tree',$res);
    }

    public function send(){
        $user=Auth::user();
        if($user==null) return redirect("/signin");
        $numbers=Input::only('phoneNumbers','message');
        echo "<pre>";
        print_r($numbers);
        echo "</pre>";
    }
}
