<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    /**
     *
     * Messages
     *
     * @return Model
     */
    public function Contacts(){
        return $this->hasMany('App\\Contact');
    }
}
