

@extends("base")

@section("head")
    @parent
    {!! HTML::style("auth/login.css") !!}
@endsection

@section('title')
    Sign In
@endsection

@section("error")
    @if (isset($error))
        <div class="alert alert-danger flash">{!! $error !!}</div>
    @endif
@endsection

@section("nav")
    @endsection

    @section("body")

        <div class="col-md-4 col-md-offset-4" style="margin-top:100px " >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row">
                        {!! HTML::image("auth/login2.png","Conxole Admin",array("class"=>"img-responsive")) !!}'
                        <h3><p style="text-align: center">پنل پیام فندق</p></h3>
                    </div>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" action="auth/login" method="post" role="form" class="form-signin">
                        {!! csrf_field() !!}
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>
                            <input class="form-control" placeholder="Email" id="username" type="text" name="email">
                            <input class="form-control" placeholder="Password" id="password" type="password" name="password">
                            <br>
                            <input style="width: 65%;float: left" class="btn btn-lg btn-success" type="submit" id="login" value="ورود »">
                            <div style="float:right" > <a href="/register" class="btn btn-lg btn-primary">ثبت نام</a></div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    @endsection
