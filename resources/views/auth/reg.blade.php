@extends("base")

@section("head")
    @parent
    {!! HTML::script("auth/reg.js") !!}
@endsection

@section("nav")
@endsection

@section('title')
    Register
@endsection

@section("body")
    <div class="container">
        <div class="col-md-4 col-md-offset-4" >
            <form class="" style="margin-top: 100px;" method="post" action="/auth/reg" role="form" style="margin-top: 100px;">
        {!! csrf_field() !!}
        <h2 style="text-align: center">ثبت نام</h2>
        <hr class="colorgraph">

        <div class="form-group">
            <input type="text" name="name" id="display_name" class="form-control input-lg" placeholder="نام " tabindex="1">
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" class="form-control input-lg" placeholder="آدرس ایمیل " tabindex="2">
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" class="form-control input-lg" placeholder="رمز عبور " tabindex="3">
        </div>
        <div class="form-group">
            <input type="password" name="re_password" id="re_password" class="form-control input-lg" placeholder="تکرار رمز عبور " tabindex="4">
        </div>


        <hr class="colorgraph">
        <div class="row">
            <div class="col-xs-12 col-md-6"><input id="sub" type="button" value="ثبت نام" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
            <div class="col-xs-12 col-md-6"><a href="/signin" class="btn btn-success btn-block btn-lg">ورود</a></div>
        </div>
    </form>
        </div>
    </div>
@endsection