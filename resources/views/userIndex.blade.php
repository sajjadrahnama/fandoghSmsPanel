@extends("base")


@section("head")
    @parent

@endsection

@section('title')
    Fandogh Panel
@endsection

@section("body")
    <div class="container" style="margin-top:70px ">

        <div class="row row-offcanvas row-offcanvas-right">
            <div class="col-md-1"></div>
            <div class="col-xs-12 col-sm-10">
                <div class=".col-md-8">
                    <table class="table table-hover" style="width: 100%;margin-top: 50px">
                        <thead>
                        <tr style="background-color: #269abc">
                            <th>ردیف</th>
                            <th>متن پیام</th>
                            <th>قیمت</th>
                            {{--<th>زمان ارسال</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @for ($i = 0; $i < count($messages); $i++)
                            @if($i%2)  {!! "<tr style='background-color: #F1F2F2'>" !!}
                                @else {!! "<tr style='background-color: #E6E7E8'>" !!}
                                @endif
                                <td>{{ $i+1 }}</td>
                                <td>{{ $messages[$i]['content'] }}</td>
                                <td>{{ $messages[$i]['price']  }}</td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>

                </div>
            </div><!--/.col-xs-12.col-sm-9-->
            <div class="col-md-1"></div>
        </div><!--/row-->

        <hr>
    </div>
@endsection