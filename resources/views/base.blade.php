<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @section('head')
        {!! HTML::style("assets/bootstrap-3.3.5-dist/css/bootstrap.min.css") !!}
        {!! HTML::style("assets/bootstrap-rtl.min.css") !!}
        {!! HTML::style("assets/base.css") !!}

    @show

    <title>@yield('title')</title>

</head>
<body>
@section("nav")
    <nav class="navbar navbar-fixed-top navbar-inverse">
        <div class="row container">
            <div class="col-md-1"></div>
            <div class="col-md-2 navbar-header">
                <a class="navbar-brand" href="">پنل پیام فندق</a>
            </div>
            <div id="navbar" class="col-md-9 collapse navbar-collapse">
                <ul class=" nav navbar-nav">
                    <li>
                        <div>
                            <a href="/index"><span class="glyphicon glyphicon-list" style="margin-bottom: 10px" aria-hidden="true"></span><p style="float: right">لیست پیام ها</p></a>

                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="/message"><span class="glyphicon glyphicon-envelope" style="margin-bottom: 10px" aria-hidden="true"></span><p style="float: right">ارسال پیام</p></a>

                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="/contacts"><span class="glyphicon glyphicon-user" style="margin-bottom: 10px" aria-hidden="true"></span><p style="float: right">مخاطبین</p></a>

                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="/support"><span class="glyphicon glyphicon-wrench" style="margin-bottom: 10px" aria-hidden="true"></span><p style="float: right">پشتیبانی</p></a>

                        </div>
                    </li>

                </ul>
                <ul class=" nav">
                    <li style="float: left">
                        <div>
                            <a href="auth/logout"><span class="glyphicon glyphicon-log-out" style="margin-bottom: 10px" aria-hidden="true"></span><p style="float: right">خروج</p></a>
                        </div>
                    </li>
                </ul>
            </div><!-- /.nav-collapse -->
        </div><!-- /.container -->
    </nav>
@show
@yield("error")
@yield('body')
<footer class="row footer">
    <div class="col-md-4"></div>
    <a href="http://www.fandogh.org" style="text-align: center" class="col-md-4 text-muted">Fandogh Co</a>
    <div class="col-md-4"></div>
</footer>
</body>
{!! HTML::script("assets/jq.js") !!}
{!! HTML::script("assets/bootstrap-3.3.5-dist/js/bootstrap.min.js") !!}
{!! HTML::script("assets/base.js") !!}
@yield('script')
</html>