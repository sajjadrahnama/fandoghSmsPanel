@extends('base')
@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
@endsection
@section('title')
    Send Message
@endsection
@section('body')
    <div class="container" style="margin-top:100px ">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <div class="form-group">
                    <label for="InputName">پیام تست</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="InputName" id="text-test" placeholder="متن پیام" required>
                        <span class="input-group-addon"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="email" class="form-control" id="InputEmail" name="phone-test" placeholder="شماره تست" required  >
                        <span class="input-group-addon"></span>
                    </div>
                </div>
                <button type="button" class="btn btn-primary">ارسال</button>
                <div>
                    <div class="form-group">
                        <label for="InputMessage">متن پیام</label>
                        <div class="input-group">
                            <textarea name="InputMessage" id="message" class="form-control" rows="5" required></textarea>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InputReal">شماره تلفن</label>
                        <div class="input-group">
                            <textarea type="text" class="form-control" name="numbers" id="numbers" rows="3" required></textarea>
                            <span class="input-group-addon"></span>
                        </div>
                    </div>
                    <div class="row">
                        <h2>انتخاب مخاطبین</h2>
                        <div class="col-sm-7">
                            <div id="treeview-checkable" class=""></div>
                        </div>

                        <div class="col-sm-4 col-sm-offset-1">
                            <h5>&nbsp;</h5>
                            <div class="form-group">
                                <button type="button" class="btn btn-success select-node" id="selectall">انتخاب همه</button>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-danger select-node" id="unselectall">عدم انتخاب همه</button>
                            </div>
                            <input style="width: 80px;text-align: center" type="submit"  value="ارسال" id="send" class="btn btn-primary select-node">
                        </div>
                    </div>
                </div>
                <form  action="/send" method="post" id="form">
                    {!! csrf_field() !!}
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {!! HTML::script("assets/bootstrap-treeview/bootstrap-treeview.min.js") !!}
    <script>
        var defaultData={!! $tree !!};

        var $checkableTree ;
        $(function() {
            $checkableTree = $('#treeview-checkable').treeview({
                data: defaultData,
                showIcon: false,
                showCheckbox: true,
                onNodeUnchecked:function(event,data){
                    $checkableTree.treeview('uncheckNode',data.nodeId);
                    var nodes=data.nodes;
                    if(nodes){
                        nodes.forEach(function (element) {
                            $checkableTree.treeview('uncheckNode',element.nodeId);
                        });
                    }

                    var sib=$checkableTree.treeview('getSiblings',data.nodeId);
                    var flag=false;
                    for(var i=0;i<sib.length;i++){
                        if(sib[i].state.checked) flag=true;
                    }

                    if(!flag) {
                        var parnetId=$checkableTree.treeview('getParent',data.nodeId).nodeId;
                        if(parnetId) $checkableTree.treeview('uncheckNode', [ parnetId, { silent: true } ]);
                    }

                },
                onNodeChecked:function(event,data){
                    $checkableTree.treeview('checkNode',data.nodeId);
                    var nodes=data.nodes;
                    if(nodes){
                        nodes.forEach(function (element) {
                            $checkableTree.treeview('checkNode',element.nodeId);
                        });
                    }
                    var sib=$checkableTree.treeview('getSiblings',data.nodeId);
                    var flag=false;
                    for(var i=0;i<sib.length;i++){
                        if( !(sib[i].state.checked)) flag=true;
                    }

                    if(!flag) {
                        var parnetId=$checkableTree.treeview('getParent',data.nodeId).nodeId;
                        if(parnetId) $checkableTree.treeview('checkNode', [ parnetId, { silent: true } ]);
                    }
                }
            });
            $checkableTree.treeview('collapseAll');
            $("#selectall").click(function(){
                $checkableTree.treeview('checkAll',[{ silent: true } ]);
            });
            $("#unselectall").click(function(){
                $checkableTree.treeview('uncheckAll',[{ silent: true } ]);
            });

        });
        var send=function(){
            var nodes=$checkableTree.treeview('getChecked');
            var form="";
            nodes.forEach(function(data){
                if(data.nodes || data.text.split(" ")[2]==='undefined') return;
                form+=("\n"+'<input type="hidden" name="phoneNumbers[]" value='+data.text.split(" ")[2]+'>');
            });
            form+=("\n"+'<input type="hidden" name="message" value="'+$("#message").val()+'">');

            var numbers=$("#numbers").val();
            numbers.split("\n").forEach(function(d){
                if(d.match("[0-9]{11,15}")!= null) {
                    form+=("\n"+'<input type="hidden" name="phoneNumbers[]" value='+d+'>');
                }

            });

            $('#form').html($('#form').html()+form);
            $("#form").submit();
        }
        $("#send").click(function(){
            send();
        })

    </script>
@endsection