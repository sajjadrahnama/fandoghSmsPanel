@extends("base")


@section("head")
    @parent
@endsection

@section('title')
    Contacts
@endsection

@section("body")
    <div class="container" style="margin-top:70px ">

        <div class="row row-offcanvas row-offcanvas-right">
            <div  class="col-md-2">
                <div class="list-group" style="margin-top: 100px">
                    @foreach($cats as $cat)
                        <a href="#" onclick="contactTable({{$cat['id']}})" class="list-group-item {{$cat['id']}}">{{$cat['name']}}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class=".col-md-8">
                    <table class="table table-hover" style="width: 100%;margin-top: 50px">
                        <thead>
                        <tr style="background-color: #269abc">
                            <th>ردیف</th>
                            <th>نام</th>
                            <th>نام خانوادگی</th>
                            <th>شماره تلفن</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div><!--/.col-xs-12.col-sm-9-->
            <div class="col-md-1"></div>
        </div><!--/row-->

        <hr>
    </div>
@endsection
@section('script')
    <script>
        $(function(){
            $("a.list-group-item").first().trigger('click');
        });
    </script>
@endsection