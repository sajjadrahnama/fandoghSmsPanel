@for ($i = 0; $i < count($contacts); $i++)
@if($i%2)  {!! "<tr style='background-color: #F1F2F2'>" !!}
    @else {!! "<tr style='background-color: #E6E7E8'>" !!}
    @endif
    <td>{{ $i+1 }}</td>
    <td>{{ $contacts[$i]['first_name'] }}</td>
    <td>{{ $contacts[$i]['last_name']  }}</td>
    <td>{{ $contacts[$i]['phone_number']  }}</td>
    </tr>
@endfor