<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status=array('pending', 'sent','delivered','not-delivered');
        $message=\App\Message::all();
        $contacts=\App\Contact::all()->toArray();
        foreach($message as $m){
            \App\Status::create(array("status"=>$status[array_rand($status,1)],"message_id"=>$m->id,"contact_id"=>$contacts[array_rand($contacts,1)]['id']));
        }
    }
}
