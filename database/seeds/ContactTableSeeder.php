<?php

use Illuminate\Database\Seeder;
use App\Contact;
class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat=\App\Category::all()->toArray();
        Contact::create(array("first_name"=>"Pesehr","last_name"=>"Sabour","phone_number"=>"989368085685","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Mohammad","last_name"=>"Takbiri","phone_number"=>"989351370819","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Mahdi","last_name"=>"Yeganeh","phone_number"=>"989102129568","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Pooya","last_name"=>"Parsa","phone_number"=>"989195085164","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Mehran","last_name"=>"Dabestani","phone_number"=>"989127106696","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Masoud","last_name"=>"Bonabi","phone_number"=>"989196396585","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Saeed","last_name"=>"Dadkhah","phone_number"=>"989388835866","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Parham","last_name"=>"Taheri","phone_number"=>"989374870483","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Hessam","last_name"=>"Hedayati","phone_number"=>"989376879924","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Saman","last_name"=>"Golestani","phone_number"=>"989132421022","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Sajad","last_name"=>"Azami","phone_number"=>"989360108690","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Mostafa","last_name"=>"Okati","phone_number"=>"989382569522","category_id"=>$cat[array_rand($cat,1)]['id']));
        Contact::create(array("first_name"=>"Sajjad","last_name"=>"Rahnama","phone_number"=>"989301212007","category_id"=>$cat[array_rand($cat,1)]['id']));

    }
}
