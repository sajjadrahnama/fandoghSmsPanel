<?php

use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $user=\App\User::all()->toArray();
        $price=array(100,200);
        for($i=0;$i<200;$i++){
            \App\Message::create(array("content"=>$faker->realText(100),"send_time"=>date("Y-m-d H:i:s", time()),"price"=>$price[array_rand($price,1)],"sender_id"=>$user[array_rand($user,1)]['id']));
        }
    }
}
