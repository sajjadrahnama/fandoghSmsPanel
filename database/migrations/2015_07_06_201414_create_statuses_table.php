<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->enum('status', ['pending', 'sent','delivered','not-delivered']);
            $table->integer('message_id')->unsigned();
            $table->foreign('message_id')->references('id')->on('messages');
            $table->integer('contact_id')->unsigned();
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('statuses', function (Blueprint $table) {
            $table->dropForeign('statuses_message_id_foreign');
            $table->dropForeign('statuses_contact_id_foreign');
        });
        Schema::table('statuses', function (Blueprint $table) {
        });
        Schema::drop('statuses');
    }
}
