/**
 * Created by sajjad on 7/14/15.
 */


var contactTable;
contactTable = function (id) {
    $("a.list-group-item").each(function(){
        $(this).removeClass("active");
    });
    $("a.list-group-item"+"."+id).addClass("active");
    $("tbody").first().fadeOut(function(){
        $.ajax({url: "/contactsByCat/"+id, success: function(result){
            $("tbody").first().html(result);
            $("tbody").first().fadeIn();
        }});
    });
};
